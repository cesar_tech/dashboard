import React from 'react';
import { fetchUtils, Admin, Resource } from 'react-admin';
import { UserList } from './users';
import dataProvider from './dataProvider';
import authProvider from './authProvider';
import simpleRestProvider from 'ra-data-simple-rest';

//authProvider={ authProvider } name="users"
/*
const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    const token = localStorage.getItem('token');
    options.headers.set('Authorization', `Bearer ${token}`);
    return fetchUtils.fetchJson(url, options);
}

const dataProvider = simpleRestProvider('https://partiu-server.herokuapp.com/agreements', httpClient);
*/

const App = () => (
    <Admin dataProvider={dataProvider} authProvider={ authProvider }> 
      <Resource name="users" list={ UserList } />
    </Admin>
  );

export default App;


