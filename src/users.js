import React from 'react';
import { List, Datagrid, TextField, EmailField, Filter, ReferenceInput, 
	TextInput, AutocompleteInput } from 'react-admin';

const UserFilter = props =>(
    <Filter {...props}>
        <TextInput label="Nome" source="nome" alwaysOn/>
        <TextInput label="Email" source="email" alwaysOn/>
        <TextInput label="CPF" source="cpf" alwaysOn/>
    </Filter>
)

//rowClick="edit"
export const UserList = props => (
		<List { ...props } rowClick="edit" filters={<UserFilter/>} bulkActionButtons={false} title="Usuários">
			<Datagrid>
				<TextField label="Id" source="id"/>
				<TextField label="Nome" source="nome"/>
				<TextField label="CPF" source="cpf" />
				<EmailField source="email" />
				<TextField label="Celular" source="celular" />
				<TextField label="Convênio" source="convenio" />
			</Datagrid>
		</List>
	);

//Modelo filtro
/*
const UserFilter = props =>(
    <Filter {...props}>
        <TextInput label="Nome" source="nome" alwaysOn/>
        <TextInput label="Email" source="email" alwaysOn/>
        <TextInput label="CPF" source="cpf" alwaysOn/>
        <ReferenceInput label="Convênio" source="id_convenio" reference="agreements" alwaysOn>
            <AutocompleteInput optionText="nome"/>
        </ReferenceInput>
    </Filter>
)
*/