export default {
    url: 'https://partiu-server.herokuapp.com',
    newUrl: 'https://partiu-server.herokuapp.com/agreements/users/',
    testUrl: 'https://partiu-server.herokuapp.com/agreements'
}